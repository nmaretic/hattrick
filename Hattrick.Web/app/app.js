(function () {
  var app = angular.module('hattrickApp', ['ngRoute']);

  app.config(["$routeProvider",
    function ($routeProvider) {
      $routeProvider
        .when("/home", {
          templateUrl: "app/Home.html",
          controller: "HomeController",
          controllerAs: "view"
        })
        .otherwise({
          redirectTo: "/home"
        });
    }
  ]);
})();