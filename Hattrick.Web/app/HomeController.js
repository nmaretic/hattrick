(function () {

  var app = angular.module('hattrickApp');
  app.controller("HomeController", ["$scope", "$route", "$log", "dataService",
    function ($scope, $route, $log, dataService) {

      var vm = this;

      vm.ticket = {
        BetValue: 0,
        ExpectedReturn: 0,
        Bets: []
      };
      vm.totalCoefficient = 0;

      vm.findOffer = function (game, tip) {
        for (offer of game.Offers) {
          if (offer.Tip == tip) {
            return offer;
          }
        }
        return null;
      }

      vm.selectOffer = function (game, offer) {
        if (offer) {
          if (!game.selectedOffer) {
            addBet(null, game, offer);
          } 
          else if (game.selectedOffer == offer.Id) {
            removeBet(game);
            vm.totalCoefficient = vm.ticket.Bets.length > 0 ? vm.ticket.Bets.reduce(reducer, 1) : 0;
            offer.isSelected = false;
            game.selectedOffer = null;
          } 
          else {
            let removeIndex = removeBet(game);
            let currentSelectedOffer = game.Offers.find((offer) => offer.isSelected == true);
            currentSelectedOffer.isSelected = false;
            addBet(removeIndex, game, offer);
          }
        }
      }

      vm.submitForm = function () {
        vm.ticket.ExpectedReturn = Number((vm.totalCoefficient * vm.ticket.BetValue).toFixed(2));
        dataService.sendTicket(vm.ticket)
          .then(function (results) {
            $log.info(`Ticket created (Id =${results.id})`);
            $route.reload();
          })
          .catch(function (errorMessage) {
            $log.error(`Error: ${errorMessage}`);
          });
      }

      dataService.getAllGames()
      .then(getGamesSuccess)
      .catch(getGamesError);
      
      function getGamesSuccess(allGames) {
        vm.allGames = allGames.map((game) => {
          game.selectedOffer = null
          game.Offers = game.Offers.map((offer) => {
            offer.isSelected = false;
            return offer;
          })
          return game;
        });
      }

      function getGamesError(errorMessage) {
        $log.error(`Error: ${errorMessage}`);
      }

      function Bet(offerId, coefficient, awayTeam, homeTeam) {
        this.OfferId = offerId
        this.Coefficient = coefficient;
        this.HomeTeam = homeTeam,
          this.AwayTeam = awayTeam
      }

      function addBet(removeIndex, game, offer) {
        let newBet = new Bet(offer.Id, offer.Coefficient, game.AwayTeam, game.HomeTeam);
        if (removeIndex !== null) {
          vm.ticket.Bets.splice(removeIndex, 0, newBet);
        } else if (vm.ticket.Bets.length < 25) {
          vm.ticket.Bets.push(newBet);
        } else {
          return;
        }
        vm.totalCoefficient = vm.ticket.Bets.reduce(reducer, 1);
        game.selectedOffer = offer.Id;
        offer.isSelected = true;
      }

      function removeBet(game) {
        let removeIndex = vm.ticket.Bets.findIndex(bet => bet.OfferId == game.selectedOffer);
        vm.ticket.Bets.splice(removeIndex, 1);
        return removeIndex;
      }

      function reducer(sum, bet) {
        return sum * bet.Coefficient;
      }

    }
  ]);
})();