(function(){
  angular.module('hattrickApp')
    .factory('dataService', ['$http','$q', dataService]);

  function dataService($http, $q){
    return {
      getAllGames: getAllGames,
      sendTicket : sendTicket
    }
    
    function sendResponseData(response){
      return response.data;
    }

    function getAllGames(){
      return $http.get('api/games')
        .then(response => response.data)
        .catch(response => $q.reject(response.data));
    }


    function sendTicket(ticket){
      return $http.post('api/ticket', ticket)
        .then(response => response.data)
        .catch(response => $q.reject(response.data));
    }
  }
})();