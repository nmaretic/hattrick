﻿namespace Hattrick.Web.Models
{
    public class Bet
    {
        public double Coefficient { get; set; }
        public int TicketId { get; set; }
        public int OfferId { get; set; }
    }
}
