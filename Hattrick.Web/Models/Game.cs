﻿using System.Collections.Generic;

namespace Hattrick.Web.Models
{
    public class Game
    {
        public Game()
        {
            Offers = new List<Offer>();
        }
        public int Id { get; set; }
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public List<Offer> Offers { get; set; }
    }
}
