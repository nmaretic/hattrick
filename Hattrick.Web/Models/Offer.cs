﻿using Hattrick.Database.EDM;

namespace Hattrick.Web.Models
{

    public class Offer
    {
        public int Id { get; set; }
        public TipType Tip { get; set; }
        public double Coefficient { get; set; }
        public int GameId { get; set; }
    }
}
