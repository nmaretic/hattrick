﻿using System;
using System.Collections.Generic;

namespace Hattrick.Web.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime BetTime { get; set; }
        public double BetValue { get; set; }
        public double ExpectedReturn { get; set; }
        public List<Bet> Bets { get; set; }
    }
}
