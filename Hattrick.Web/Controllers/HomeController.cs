﻿using System.Web.Mvc;

namespace Hattrick.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Hattrick";
            return View();
        }
    }
}
