﻿using edm = Hattrick.Database.EDM;
using Hattrick.Web.Models;
using Hattrick.Infrastructure.Repositories;
using System.Collections.Generic;
using Hattrick.Infrastructure.Services;
using System.Web.Http;
using System.Net;
using System;
using System.Data.Entity;
using System.Linq;

namespace Hattrick.Web.Controllers
{
    [RoutePrefix("api")]
    public class ValuesController : ApiController
    {
        private UnitOfWork unitOfWork;
        private DataService dataService;
        private LogService logService;
        public ValuesController()
        {
            unitOfWork = new UnitOfWork();
            dataService = new DataService();
            logService = new LogService();
        }
        [Route("games")]
        public List<Game> GetAllGames()
        {
            var model = unitOfWork.Games.Query().Include("Offers")
                .Select(dbGame => new Game()
                {
                    Id = dbGame.Id,
                    AwayTeam = dbGame.AwayTeam,
                    HomeTeam = dbGame.HomeTeam,
                    Offers = dbGame.Offers.Select(dbOffer => new Offer()
                    {
                        Id = dbOffer.Id,
                        Tip = dbOffer.Tip,
                        Coefficient = dbOffer.Coefficient,
                        GameId = dbOffer.GameId
                    }).ToList()
                }).ToList();
            
            return model;
        }

        [HttpPost, Route("ticket")]
        public IHttpActionResult CreateTicket(edm.Ticket newTicket)
        {
            newTicket.BetTime = DateTime.Now;
            newTicket.HashCode = dataService.SetHashCode(newTicket);
            bool success = dataService.ValidateTicket(newTicket, unitOfWork);
            if (!success)
            {
                logService.UpdateLog(success, newTicket);
                return Content(HttpStatusCode.Conflict, "ticket rejected");
            }
            unitOfWork.Tickets.Add(newTicket);
            unitOfWork.Complete();
            logService.UpdateLog(success, newTicket);
            var model = new { id = newTicket.Id };
            return Created(Request.RequestUri, model);
        }
    }
}
