USE [Hattrick]
GO
/****** Object:  Table [dbo].[Bet]    Script Date: 1/17/2018 11:36:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bet](
	[Coefficient] [float] NOT NULL,
	[Ticket_id] [int] NOT NULL,
	[Offer_id] [int] NOT NULL,
 CONSTRAINT [PK_Bet] PRIMARY KEY CLUSTERED 
(
	[Ticket_id] ASC,
	[Offer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Game]    Script Date: 1/17/2018 11:36:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Game](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Home_team] [nvarchar](30) NOT NULL,
	[Away_team] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Offer]    Script Date: 1/17/2018 11:36:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tip] [int] NOT NULL,
	[Coefficient] [float] NOT NULL,
	[Game_id] [int] NOT NULL,
 CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 1/17/2018 11:36:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ticket](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Bet_time] [datetime] NOT NULL,
	[Bet_value] [float] NOT NULL,
	[Expected_return] [float] NOT NULL,
	[Hash_code] [int] NOT NULL,
 CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Game] ON 

INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (1, N'FC Barcelona', N'Levante UD')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (2, N'Ath.Bilbao', N'Dep.Alaves')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (3, N'Villareal', N'Dep.LaCoruna')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (4, N'Celta Vigo', N'Real Madrid')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (5, N'Hajduk', N'Dinamo')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (6, N'Rijeka', N'Osijek')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (7, N'Hapoel Haifa', N'Hap. Ashkelon')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (8, N'Lens', N'US Boulogne')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (9, N'Man. Utd.', N'West Ham')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (10, N'Stoke City', N'Norwich')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (11, N'B.Dortmunt', N'Vfl Wolfsburg')
INSERT [dbo].[Game] ([Id], [Home_team], [Away_team]) VALUES (12, N'ParisSG', N'Dijon')
SET IDENTITY_INSERT [dbo].[Game] OFF
SET IDENTITY_INSERT [dbo].[Offer] ON 

INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (6, 0, 2.3, 1)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (7, 1, 3.5, 1)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (8, 2, 2.5, 1)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (9, 3, 1.4, 1)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (10, 4, 1.45, 1)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (11, 5, 1.2, 1)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (12, 0, 1.9, 2)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (13, 1, 3.6, 2)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (14, 2, 3.1, 2)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (15, 3, 1.25, 2)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (16, 4, 1.65, 2)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (17, 5, 1.2, 2)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (18, 5, 1.3, 3)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (19, 4, 1.5, 3)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (20, 3, 1.22, 3)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (21, 2, 2.7, 3)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (22, 1, 3.95, 3)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (23, 0, 1.7, 3)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (24, 0, 2.45, 4)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (26, 1, 3.15, 4)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (27, 2, 2.55, 4)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (28, 3, 2.6, 4)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (29, 4, 1.4, 4)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (30, 5, 1.25, 4)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (31, 0, 2.2, 5)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (32, 1, 4.5, 5)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (33, 2, 2.75, 5)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (34, 3, 1.7, 5)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (35, 4, 2.35, 5)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (36, 5, 1.6, 5)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (38, 0, 1.3, 6)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (39, 1, 3.25, 6)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (40, 2, 3.4, 6)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (41, 3, 1.25, 6)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (42, 4, 3, 6)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (43, 5, 1.1, 6)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (44, 0, 2.7, 7)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (45, 1, 2.5, 7)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (46, 2, 2.05, 7)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (47, 3, 2.25, 7)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (48, 4, 1.9, 7)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (49, 5, 1.35, 7)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (50, 0, 2.8, 8)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (51, 1, 2.9, 8)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (52, 2, 2.35, 8)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (53, 3, 2.25, 8)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (54, 4, 2, 8)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (55, 5, 1.75, 8)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (56, 0, 1.3, 9)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (57, 1, 3.5, 9)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (58, 2, 5.7, 9)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (59, 3, 1.25, 9)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (60, 4, 5, 9)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (61, 0, 2.6, 10)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (62, 1, 3.75, 10)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (64, 2, 2.05, 10)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (66, 3, 2.25, 10)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (67, 5, 1.55, 10)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (68, 0, 1.6, 11)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (69, 1, 3.55, 11)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (70, 2, 2.8, 11)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (71, 3, 1.45, 11)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (72, 4, 2.5, 11)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (74, 5, 1.9, 11)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (75, 0, 2.35, 12)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (76, 1, 3.7, 12)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (77, 2, 3.2, 12)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (78, 3, 2.05, 12)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (79, 4, 3.05, 12)
INSERT [dbo].[Offer] ([Id], [Tip], [Coefficient], [Game_id]) VALUES (80, 5, 2.25, 12)
SET IDENTITY_INSERT [dbo].[Offer] OFF
ALTER TABLE [dbo].[Bet]  WITH CHECK ADD  CONSTRAINT [FK_Offer_Bet] FOREIGN KEY([Offer_id])
REFERENCES [dbo].[Offer] ([Id])
GO
ALTER TABLE [dbo].[Bet] CHECK CONSTRAINT [FK_Offer_Bet]
GO
ALTER TABLE [dbo].[Bet]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Bet] FOREIGN KEY([Ticket_id])
REFERENCES [dbo].[Ticket] ([Id])
GO
ALTER TABLE [dbo].[Bet] CHECK CONSTRAINT [FK_Ticket_Bet]
GO
ALTER TABLE [dbo].[Offer]  WITH CHECK ADD  CONSTRAINT [FK_Game_Offer] FOREIGN KEY([Game_id])
REFERENCES [dbo].[Game] ([Id])
GO
ALTER TABLE [dbo].[Offer] CHECK CONSTRAINT [FK_Game_Offer]
GO
