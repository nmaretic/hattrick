﻿using Hattrick.Database.EDM;
using Hattrick.Infrastructure.Repositories;
using System.Linq;

namespace Hattrick.Infrastructure.Services
{
    public class DataService
    {
        public bool ValidateTicket(Ticket newTicket, UnitOfWork unitOfWork)
        {
            if (newTicket.Bets.Count > 25)
            {
                return false;
            }
            var identicalTickets = unitOfWork.Tickets.Query()
                .Where(ticket => ticket.HashCode == newTicket.HashCode).ToList();
            var expectedReturnSum = identicalTickets.Aggregate<Ticket, double>
                (0, (sum, t) => sum + t.ExpectedReturn);
            if (expectedReturnSum  > 1000)
            {
                return false;
            }
            return true;
        }

        public int SetHashCode(Ticket ticket)
        {
            var offers = from bet in ticket.Bets orderby bet.OfferId select bet.OfferId;
            int hashCode = 17;
            foreach (var offer in offers)
            {
                unchecked
                {
                    hashCode = hashCode * 23 + offer.GetHashCode();
                }
            }
            return hashCode;
        }
    }
}
