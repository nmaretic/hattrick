﻿using db = Hattrick.Database.EDM;
using System;
using System.IO;

namespace Hattrick.Infrastructure.Services
{
    public  class LogService
    {
        public void UpdateLog(bool success, db.Ticket ticket)
        {
            string dirPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
            Directory.CreateDirectory(dirPath + "/Hattrick");
            var filePath = dirPath + "/Hattrick/log.txt";
            string message = success ? $"ticket added (Id = {ticket.Id})" : "ticket rejected";
            string log = $"{message} at {ticket.BetTime} ";
            File.AppendAllText(filePath, log + Environment.NewLine);
        }
    }
}