﻿using Hattrick.Database.EDM;

namespace Hattrick.Infrastructure.Repositories
{

    public class UnitOfWork
    {
        
        private readonly HattrickEntities _context;
        public UnitOfWork()
        {
            _context = new HattrickEntities();
            Games = new Repository<Game>(_context);
            Offers = new Repository<Offer>(_context);
            Tickets = new Repository<Ticket>(_context);
        }
        public Repository<Game> Games { get; private set; }

        public Repository<Offer> Offers { get; private set; }

        public Repository<Ticket> Tickets { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }
    }
}
